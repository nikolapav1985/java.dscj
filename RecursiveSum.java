/**
*
* RecursiveSum class
*
* example of recursively (or inductively) defined expression
*
* compute a sum of natural numbers
*
* if n=1 then sum=1
*
* if n>1 then sum=n+(n-1)+(n-2)+...+1
*
* alternatively sum of natural numbers can be found using expression n(n+1)(1/2)
*
* proof also given inductively
*
* ----- proof -----
*
* n=1,sum=1,n=2,sum=1+2=3,n=k,sum=k(k+1)(1/2)
* n=k+1,sum=k(k+1)(1/2)+k+1=(k+1)(k(1/2)+1)=(k+1)(k+2)(1/2)
*
* ----- compile -----
*
* javac RecursiveSum.java
*
* ----- run -----
*
* java RecursiveSum 5
*
* ----- example output -----
*
* 15
*
*/
class RecursiveSum{
    public static int sum(int n){ // n>=1
        if(n==1){
            return n;
        } else {
            return n+sum(n-1);
        }
    }
    public static void main(String args[]){
        System.out.println(sum(Integer.parseInt(args[0]))); // pass a command line argument e.g. java RecursiveSum 5
    }
}
