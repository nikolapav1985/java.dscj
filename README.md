Discussion 2
------------

- RecursiveSum.java (file, recursion example, used to find sum of natural numbers)
- More details available in comments

Test environment
----------------

- os lubuntu 16.04 kernel version 4.13.0
- javac version 11.0.5
